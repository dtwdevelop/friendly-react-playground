import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
// import  Data from './Data';
import List from './List';
// import  {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import  {change,SortList}  from './actions/app.actions'

class App extends Component {
    constructor(props){
        super(props)
       this.state = {
            list: '',
           status: ''
       }
      this.sortMessage  = this.sortMessage.bind(this);
    }

    sortMessage(event){

        if(this.state){

           this.props.SortList(event.target.value)
        }
        event.preventDefault();
    }

  render() {
      return (
      <div className="App">

        <header className="App-header">

          <h1 className="App-title">Task</h1>
        </header>
        <p className="App-intro"></p>

          <form >
              <select name="sort" value={this.state.status} onChange={this.sortMessage}>
                  <option  value="null">Select</option>
                  <option  value="id">By id</option>
                  <option value="name">By name</option>
              </select>
          </form>
          <List data={this.props.data} />
          {/*<Data/>*/}
      </div>
    );
  }
}

const mapStateToProps = (state)=>{

    return {
        data: state
    }
}

export default  connect(mapStateToProps,{change,SortList})(App);

