import  React ,{Component }from 'react';
import {add} from './actions/app.actions'
import {connect} from 'react-redux'

class Forms extends Component {
    constructor(props){
        super(props)
        this.state ={}
        this.sendMessage  = this.sendMessage.bind(this);
    }

    componentDidMount() {

    }
    sendMessage(event){
        let val = event.target.elements.message.value;
        if(val !== ''){
            this.props.add(val)
        }
        event.preventDefault();
    }

    render(){
        return(
           <div>
               <form onSubmit={this.sendMessage}>
                   <p>Your Tasks</p>
                <input type="text" name="message"/>

                <input type="submit"  value="Send"  />
            </form>
           </div>

        );
    }

}

export default connect(null,{add})(Forms)
