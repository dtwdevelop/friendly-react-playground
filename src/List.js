/**
 * Created by hider on 12/11/2017.
 */
import React ,{Component} from 'react'
import  {change, deleteById }  from './actions/app.actions'
import {connect} from 'react-redux'
import Forms from './Forms'

class List extends Component {
    constructor(props){
        super(props)
       this.state ={}
    }
    deleteMessage(id){
        console.log(id)
       this.props.deleteById(id)
    }
    updateMessage(id){
        this.props.change(id,'New message');

    }

    render(){
        const list = this.props.data.map(value=><li key={value.id}>
            <button onClick={()=>this.updateMessage(value.id)}>Update</button>
            <button onClick={()=>this.deleteMessage(value.id)}>x</button>id: {value.id} message:{value.message}</li>);
        return (<div> <ul>
               { list }

            </ul>
                <Forms  />
            </div>


        );
    }
}

export default connect(null,{change,deleteById})(List)
