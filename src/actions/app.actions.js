import {ADD_BLOG , CHANGE_BLOG,DELETE_BLOG,SORT_LIST} from  '../type/app.constants';


export const  add =  (message) =>{
    return {
        type: ADD_BLOG,
        message
    }
}

export const  change  = (id,message)=>{
   return {
       type: CHANGE_BLOG,
       id,
       message
   }
}

export  const deleteById = (id)=>{
    return {
        type:DELETE_BLOG,
        id
    }
}

export const SortList  = (sorteBy)=>{
  return {
      type: SORT_LIST,
      sorteBy
  }
}
