import {ADD_BLOG , CHANGE_BLOG,DELETE_BLOG,SORT_LIST} from  '../type/app.constants';

const add = (action)=>{
    return {
     id : Math.round(Math.random()*1000),
    message: action.message
}
}

const  change=(state=[],action)=>{
    return  state.map((value)=>{
         console.log(value)
        if(value.id === action.id)
            return {
                ...value,
                ...{message:action.message}
            }
        else{
            return value
        }
    });
}

const  deleteby=(state=[],action)=>{
    const newstate =  state.filter((value)=>{
        return value.id !== action.id
    });

    return newstate;
}

const sortById = (state=[],action)=>{
    let sorted =[] ;

    switch(action.sorteBy){
        case 'id':{

           sorted  = state.sort(compare);
            break;
        }
        case 'name':{

            sorted  = state.sort(SortbyName);
           break
        }
    }
    return [...sorted]

}
function SortbyName(a,b){
    if (a.message < b.message) {
        return -1;
    }
    if (a.message > b.message ) {
        return 1;
    }
    return 0;
}

function compare(a, b) {
    if (a.id < b.id) {
        return -1;
    }
    if (a.id > b.id ) {
        return 1;
    }
    return 0;
}

export  function app_blog(state=[],action){
    switch(action.type) {
        case ADD_BLOG:{

         let  newstate =  [...state,add(action)]
            return newstate;
        }
        case CHANGE_BLOG:{
            return  change(state,action);
        }
        case DELETE_BLOG:{
            return deleteby(state,action);
        }
        case SORT_LIST:{
           return sortById(state,action)
        }
        default:{
            return state
        }
    }
}
